# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Burster Presicion Resistance Decade 1427
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory

import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def mainProcedure():
    try:
        # -----------------------------------------------------------------------------
        # Input PVs
        # -----------------------------------------------------------------------------
        functionRBV = int(PVUtil.getString(pvs[0]))         # $(DEV):function_RBV
        unitRBV     = int(PVUtil.getString(pvs[1]))         # $(DEV):unit_RBV
        # -----------------------------------------------------------------------------
        status_meaning = ''
        # -----------------------------------------------------------------------------
        # Decade function setting
        # F <CPD> { 0 | 1 | 2 | 3 | 4 | 5 | S | O }
        #     0:   resistance mode
        #     1:   Pt(68) temperature sensor simulation
        #     2:   Pt(90) temperature sensor simulation
        #     3:   Pt(US) temperature sensor simulation
        #     4:   Ni temperature sensor simulation
        #     5:   Custom temperature sensor simulation
        #     'S': Short simulation (extra ordered option)
        #     'O': Open simulation (extra ordered option)
        # -----------------------------------------------------------------------------
        if functionRBV == 0:
            status_meaning += 'Func(0): resistance mode; '
        elif functionRBV == 1:
            status_meaning += 'Func(1): Pt(68) temperature sensor simulation; '
        elif functionRBV == 2:
            status_meaning += 'Func(2): Pt(90) temperature sensor simulation; '
        elif functionRBV == 3:
            status_meaning += 'Func(3): Pt(US) temperature sensor simulation; '
        elif functionRBV == 4:
            status_meaning += 'Func(4): Ni temperature sensor simulation; '
        elif functionRBV == 5:
            status_meaning += 'Func(5): Custom temperature sensor simulation; '
        # -----------------------------------------------------------------------------
        # Temperature unit setting (T.unit)
        # U <CPD> { 0 | 1 }
        #     0: sets degrees of Celsius (°C)
        #     1: sets degrees of Fahrenheit (°F)
        # -----------------------------------------------------------------------------
        if unitRBV == 0:
            status_meaning += 'Unit(0): Celsius (o.C);'
        elif unitRBV == 1:
            status_meaning += 'Unit(1): Fahrenheit (o.F);'
        # -----------------------------------------------------------------------------
        pvs[2].setValue(status_meaning)
    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
mainProcedure()